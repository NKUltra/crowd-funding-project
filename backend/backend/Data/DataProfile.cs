﻿using AutoMapper;
using backend.DataModel;
using backend.Model;

namespace backend.Data
{
    public class DataProfile : Profile
    {
        public DataProfile()
        {
            CreateMap<FundingData, Funding>();
            CreateMap<InvestmentData, Investment>();
        }
    }
}
