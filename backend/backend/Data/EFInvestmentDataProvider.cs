﻿using AutoMapper;
using backend.DataModel;
using backend.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Data
{
    public class EFInvestmentDataProvider : DbContext, IInvestmentDataProvider
    {
        private readonly IMapper _mapper;

        public EFInvestmentDataProvider(IMapper mapper, DbContextOptions<EFInvestmentDataProvider> options) : base(options)
        {
            _mapper = mapper;
        }

        public DbSet<InvestmentData> InvestmentDBset { get; set; }

        [Authorize]
        public Investment GetInvestment(int id)
        {
            return _mapper.Map<Investment>(InvestmentDBset.SingleOrDefault(i => i.ID == id));
        }

        [Authorize]
        public async Task<int> PostInvestment(Investment investment, string userId)
        {
            investment.UserId = userId;
            var i = _mapper.Map<InvestmentData>(investment);
            if(InvestmentDBset.Where(x => x.UserId == userId && x.FundingId == i.FundingId).Any()){
                return -1;
            }

            InvestmentDBset.Add(i);
            int id = await SaveChangesAsync();
            return id;
        }

        [Authorize]
        public IEnumerable<Investment> GetAllUserInvestments(string UserId)
        {
            return InvestmentDBset.Where(i => i.UserId == UserId).Select(f => _mapper.Map<Investment>(f));
        }
    }
}
