﻿using backend.Model;
using System;
using System.Collections.Generic;

namespace backend.Data
{
    public interface IFundingDataProvider :IDisposable
    {        
        Funding GetFunding(int id, string userId);
        IEnumerable<Funding> GetFundings(string userId);
    }
}