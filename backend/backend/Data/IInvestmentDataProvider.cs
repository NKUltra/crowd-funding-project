﻿using backend.DataModel;
using backend.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace backend.Data
{
    public interface IInvestmentDataProvider : IDisposable
    {
        DbSet<InvestmentData> InvestmentDBset { get; set; }

        Investment GetInvestment(int fundingId);

        IEnumerable<Investment> GetAllUserInvestments(string userId);

        Task<int> PostInvestment(Investment investment, string userId);

    }
}