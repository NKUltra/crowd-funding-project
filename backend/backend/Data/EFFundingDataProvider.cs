﻿using AutoMapper;
using backend.DataModel;
using backend.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace backend.Data
{
    public class EFFundingDataProvider : DbContext, IFundingDataProvider
    {
        private readonly IMapper _mapper;
        private readonly IInvestmentDataProvider _efi;

        public EFFundingDataProvider(IMapper mapper, IInvestmentDataProvider efi, DbContextOptions<EFFundingDataProvider> options) : base(options)
        {
            _mapper = mapper;
            _efi = efi;
            if (!FundingsDbSet.Any())
            {
                for (int i = 1; i <= 25; i++)
                {
                    FundingsDbSet.Add(new FundingData() { Name = $"Funding {i}", Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed magna sit amet purus pulvinar eleifend. Vivamus mollis vitae purus eget pharetra. Aliquam malesuada dolor vel aliquam tristique. Aenean non nibh erat. Proin ullamcorper mi id mauris congue ullamcorper. Nullam eget orci nec quam elementum interdum. Duis rhoncus ligula elit, in semper lacus finibus eget." });
                }

                SaveChanges();
            }
        }

        public DbSet<FundingData> FundingsDbSet { get; set; }

        public Funding GetFunding(int id, string userId)
        {
            Funding result;// = Fundings.Select(f =>  _mapper.Map<Funding>(f));

            if (String.IsNullOrEmpty(userId))
            {
                result = GetFunding(id);

            }
            else
            {
                result = GetFundingForLoggedInUser(id, userId);
            }
            return result;
        }

        private Funding GetFunding(int id)
        {

            return _mapper.Map<Funding>(FundingsDbSet.Where(x => x.ID == id).SingleOrDefault());
        }

        [Authorize]
        private Funding GetFundingForLoggedInUser(int id, string userId)
        {
            var investment = _mapper.Map<Investment>(_efi.InvestmentDBset.Where(x => x.UserId == userId && x.FundingId == id).SingleOrDefault());
            var ret = _mapper.Map<Funding>(FundingsDbSet.Where(x => x.ID == id).SingleOrDefault());
            ret.Investment = investment;
            return ret;
        }

        
        public IEnumerable<Funding> GetFundings(string userId)
        {
            IEnumerable<Funding> result;// = Fundings.Select(f =>  _mapper.Map<Funding>(f));

            if (String.IsNullOrEmpty(userId))
            {
                result = GetFundings();

            }
            else
            {
                result = GetAllFundingsForLoggedInUser(userId);
            }
            return result;
        }

        private IEnumerable<Funding> GetFundings()
        {
            var result = FundingsDbSet.Select(f => _mapper.Map<Funding>(f));

            return result;
        }

        [Authorize]
        private IEnumerable<Funding> GetAllFundingsForLoggedInUser(string userId)
        {
            /*ToList() is needed otherwise "Value cannot be null. Parameter name: entityType" is thrown. 
             * Found solutin here: https://stackoverflow.com/questions/53868550/ef-core-join-from-two-iqueryables-throws-system-argumentnullexception            
             */

            var investmentsList = _efi.InvestmentDBset.ToList();

            var result = from funding in FundingsDbSet
                         join investment in investmentsList on 
                            new { fundingID = funding.ID, userId } 
                         equals 
                            new { fundingID = investment.FundingId, userId = investment.UserId } 
                         into investmentGroup
                         from item in investmentGroup.DefaultIfEmpty()
                         select new Funding { ID = funding.ID, Name = funding.Name, Text = funding.Text, Investment = item == null ? null : _mapper.Map<Investment>(item) };

            return result;
        }
    }
}
