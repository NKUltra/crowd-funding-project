﻿using backend.Data;
using backend.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FundingsController : ControllerBase
    {
        private readonly IFundingDataProvider _fundingDataProvider;

        public FundingsController(IFundingDataProvider fundingDataProvider)
        {
            _fundingDataProvider = fundingDataProvider;
        }

        //[Authorize]
        // GET: api/Fundings/id
        [HttpGet("{id}")]
        public IActionResult GetFunding([FromRoute] int id)
        {
            var userId = HttpContext.User.Claims.FirstOrDefault() == null ? null : HttpContext.User.Claims.First().Value;

            var funding = _fundingDataProvider.GetFunding(id, userId);

            if (funding == null)
            {
                return NotFound();
            }
            return Ok(funding);
        }


        // GET: api/Fundings
        [HttpGet]
        public IEnumerable<Funding> GetFundings()
        {
            var userId = HttpContext.User.Claims.FirstOrDefault() == null ? null : HttpContext.User.Claims.First().Value;
            return _fundingDataProvider.GetFundings(userId);
        }    
    }
}