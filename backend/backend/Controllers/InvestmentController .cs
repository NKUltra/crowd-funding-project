﻿using backend.Data;
using backend.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvestmentController  : ControllerBase
    {
        private readonly IInvestmentDataProvider _investmentDataProvider;

        public InvestmentController (IInvestmentDataProvider investmentDataProvider)
        {
            _investmentDataProvider = investmentDataProvider;
        }        

        // GET: api/Fundings/5
        [Authorize]
        [HttpGet("{id}")]
        public IActionResult GetInvestment([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var investment = _investmentDataProvider.GetInvestment(id);

            if (investment == null)
            {
                return NotFound();
            }

            return Ok(investment);
        }

        // POST: api/Investment
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> PostFunding([FromBody] Investment investment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var userId = HttpContext.User.Claims.First().Value;
            var result = await _investmentDataProvider.PostInvestment(investment, userId);

            if(result == -1)
            {
                return BadRequest();
            }
            investment.ID = result;
            var ret = CreatedAtAction("GetInvestment", new { id = result }, investment);
            return ret;
        }        
    }
}