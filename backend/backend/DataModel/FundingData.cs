﻿namespace backend.DataModel
{
    public class FundingData
    {
        public int ID { get; set; }
        public string Name{ get; set; }
        public string Text { get; set; }
    }
}
