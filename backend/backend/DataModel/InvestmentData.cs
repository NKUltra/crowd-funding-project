﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace backend.DataModel
{
    public class InvestmentData
    {
        [Key]
        public int ID { get; set; }
        [Range(100, 10000)]
        [Required]
        public decimal Value { get; set; }
        [Required]
        [ForeignKey("FundingData")]
        public int FundingId { get; set; }
        [Required]
        public string UserId { get; set; }

    }
}
