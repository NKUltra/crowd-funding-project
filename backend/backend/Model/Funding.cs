﻿namespace backend.Model
{
    public class Funding
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public Investment Investment { get; set; }
    }
}
