﻿using System.ComponentModel.DataAnnotations;

namespace backend.Model
{
    public class Investment
    {
        public int ID { get; set; }
        [Range(100,10000)]
        [Required]
        public decimal Value { get; set; }
        [Required]
        public int FundingId { get; set; }
        public string UserId { get; set; }

    }
}
