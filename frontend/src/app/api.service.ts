import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { Investment } from './investment';

@Injectable()
export class ApiService {

    private selectedInvestment = new Subject<Investment>();
    investmentSelected = this.selectedInvestment.asObservable();

    constructor(private http: HttpClient) { }

    getFunding<Funding>(id) {
        return this.http.get<Funding>(`http://localhost:63100/api/fundings/${id}`);
    }

    getFundings() {
        return this.http.get('http://localhost:63100/api/fundings');
    }

    getInvestment(id) {
        return this.http.get(`http://localhost:63100/api/investment/${id}`);
    }
    postInvestment(investment) {
        this.http.post('http://localhost:63100/api/investment', investment).subscribe(res => {
            return (res);
        });
    }

    selectInvestment(investment) {
        this.selectedInvestment.next(investment);
    }
}

