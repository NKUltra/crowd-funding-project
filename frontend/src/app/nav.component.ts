import { Component } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
  selector: 'nav',
  template: `
    <mat-toolbar>
        <button mat-button routerLink="/"><mat-icon>home</mat-icon> Fundings</button>
        <span style="flex: 1 1 auto;"></span>
        <button *ngIf="!auth.isAuthenicated" mat-button routerLink="/register">Register</button>
        <button *ngIf="!auth.isAuthenicated" mat-button routerLink="/login">Login</button>
        <button *ngIf="auth.isAuthenicated" mat-button (click)="auth.logout()" routerLink="/">Logout</button>
    </mat-toolbar>
  `
})
export class NavComponent {
  constructor(private auth: AuthService) { }
}
