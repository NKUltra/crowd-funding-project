import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

    constructor(private http: HttpClient, private router: Router) { }

    get isAuthenicated() {
        return !!localStorage.getItem('token');
    }

    register(credentlias) {
        this.http.post<any>('http://localhost:63100/api/account', credentlias).subscribe(res =>
            this.authenticate(res)
        );
    }

    login(credentlias) {
        this.http.post<any>('http://localhost:63100/api/account/login', credentlias).subscribe(res =>
            this.authenticate(res)
        );
    }

    authenticate(res) {
        localStorage.setItem('token', res);
        this.router.navigate(['/']);
    }

    logout() {
        localStorage.removeItem('token');
        this.router.navigate(['/']);
    }
}


