import { Investment } from './investment';

export interface Funding {
    id: number;
    name: number;
    text: number;
    investment: Investment;
}
