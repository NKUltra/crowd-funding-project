import { Component, OnInit } from '@angular/core';
import { ApiService } from './api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './auth.service';

@Component({
    selector: 'fundings',
    templateUrl: './fundings.component.html'
})
export class FundingsComponent  implements OnInit {

    // question = {}
    fundings;

    constructor(private api: ApiService, private auth: AuthService, private router: Router, private route: ActivatedRoute) {
        this.router.routeReuseStrategy.shouldReuseRoute = function () {
            return false;
        };
    }

    ngOnInit() {
        this.api.getFundings().subscribe(res => {
            console.log('load');
            this.fundings = res;
        });
    }
}
