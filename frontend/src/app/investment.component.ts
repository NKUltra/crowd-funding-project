import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiService } from './api.service';
import { ActivatedRoute } from '@angular/router';
import { Funding } from './funding';

@Component({
    templateUrl: './investment.component.html'
})
export class InvestmentComponent implements OnInit {

    investment = {};
    funding = { investment: {} };
    fundingId;

    form;

    constructor(private api: ApiService, private fb: FormBuilder, private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.fundingId = this.route.snapshot.paramMap.get('fundingId');
        this.api.getFunding<Funding>(this.fundingId).subscribe(res => {
            this.funding = res;
        });

        this.api.investmentSelected.subscribe(investment => this.funding.investment = investment);
    }

    post(investment) {
        investment.fundingId = this.fundingId;
        this.api.postInvestment(investment);
        this.api.selectInvestment(investment);
    }


}
