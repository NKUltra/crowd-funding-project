export interface Investment {
    id: number;
    value: number;
    fundingId: number;
    userId: string;
}
